#include <iostream>
#include <vector>
#include <cassert>

class FibonacciModM
{
public:

	FibonacciModM(const int64_t newM) :
		M{newM}
	{}

	int64_t get(int64_t N)
	{
		assert(N >= 0);

		if (N < 2)
		{
			return N;
		}

		if (N > numbers.size() - 1)
		{
			auto oldSize{ numbers.size() };
			numbers.resize(N + 1);

			for (size_t i{ oldSize }; i <= N; ++i)
			{
				numbers[i] = (numbers[i - 2] + numbers[i - 1]) % M;
			}

			return numbers[N];
		}
		else
		{
			return numbers[N];
		}
	}

private:

	int64_t M;

	std::vector<int64_t> numbers{ 0, 1 };
};

int64_t findFibonacciModPeriod(FibonacciModM&  fibonacciMod, int64_t M)
{
	if (M < 2)
	{
		return M;
	}

	int64_t period{};

	std::vector<int64_t> sequence{};
	sequence.resize(2);

	for (int64_t i{ 0 }; i < 2; ++i)
	{
		sequence[i] = fibonacciMod.get(i);
	}

	int64_t T{ 2 };
	for (;;)
	{		
		sequence.resize(2 * T);

		for (int64_t i{ (T - 1) * 2 }; i < 2 * T; ++i)
		{
			sequence[i] = fibonacciMod.get(i);
		}

		bool isPeriodic{ true };

		for (int64_t i{ 0 }; i < T; ++i)
		{
			if (sequence[i] != sequence[i + T])
			{
				isPeriodic = false;
				break;
			}
		}

		if (true == isPeriodic)
		{
			period = T;
			break;
		}
		else
		{
			++T;
		}
	}

	return period;
}

int main()
{
	int64_t n{}, m{};

	std::cin >> n >> m;

	FibonacciModM remainders{ m };

	auto period{ findFibonacciModPeriod(remainders, m) };

	std::cout << remainders.get(n % period) << std::endl;

	return 0;
}