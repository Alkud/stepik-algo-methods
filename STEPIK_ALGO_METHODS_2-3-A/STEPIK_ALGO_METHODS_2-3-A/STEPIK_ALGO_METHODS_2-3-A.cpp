#include <cassert>
#include <cstdint>
#include <iostream>

template <typename T>
T gcd(T a, T b) {
	assert(a > 0 && b > 0);
	
	while (a != 0 && b != 0)
	{
		if (a > b)
		{
			a = a % b;
		}
		else if (a < b)
		{
			b = b % a;
		}
		else
		{
			b = 0;
		}
	}

	if (0 == a)
	{
		return b;
	}
	else
	{
		return a;
	}
}

int main(void) {
	std::int32_t a, b;
	std::cin >> a >> b;
	std::cout << gcd(a, b) << std::endl;
	return 0;
}