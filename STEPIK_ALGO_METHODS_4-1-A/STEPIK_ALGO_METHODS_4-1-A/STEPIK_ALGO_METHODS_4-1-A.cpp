#include <iostream>
#include <map>
#include <set>
#include <algorithm>
#include <iterator>

using Interval = std::pair<int64_t, int64_t>;

bool doesContain(const int64_t& point, const Interval& interval)
{
	return (point <= interval.second && point >= interval.first);
}


int main()
{
	int64_t n{}, x1{}, x2{};
	std::cin >> n;

	std::multimap<int64_t, int64_t> intervals{}; // <start, end>
	std::set<int64_t> points{};

	for (int64_t i{}; i < n; ++i)
	{
		std::cin >> x1 >> x2;

		if (x1 < x2)
		{
			intervals.emplace(x1, x2);
		}
		else
		{
			intervals.emplace(x2, x1);
		}		
	}

	std::set<int64_t> result{};
			
	while (intervals.empty() != true)
	{
		points.clear();
		for (const auto interval : intervals)
		{
			points.insert(interval.second);
		}

		auto currentPoint{ points.begin() };
		
		result.insert(*currentPoint);

		auto interval{ intervals.begin() };
		while (interval != intervals.end())
		{
			if (doesContain(*currentPoint, *interval))
			{
				intervals.erase(interval);
				interval = intervals.begin();
				continue;
			}
			++interval;
		}		
	}

	std::cout << result.size() << std::endl;
	std::copy(result.begin(), result.end(), std::ostream_iterator<int64_t>(std::cout, " "));
}