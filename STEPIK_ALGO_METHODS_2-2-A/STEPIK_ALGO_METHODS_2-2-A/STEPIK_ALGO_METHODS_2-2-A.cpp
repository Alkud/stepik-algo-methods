#include <iostream>
#include <vector>
#include <cassert>

class Fibonacci
{
public:
	static int64_t get(int64_t N)
	{
		assert(N >= 0);

		if (N < 2)
		{
			return N;
		}

		if (N > numbers.size() - 1)
		{
			auto oldSize{ numbers.size() };
			numbers.resize(N + 1);

			for (size_t i{ oldSize }; i <= N; ++i)
			{
				numbers[i] = numbers[i - 2] + numbers[i - 1];
			}

			return numbers[N];
		}
		else
		{
			return numbers[N];
		}
	}

private:
	static std::vector<int64_t> numbers;
};

std::vector<int64_t> Fibonacci::numbers { 0, 1 };

int main()
{
	int64_t n{};

    std::cin >> n;

	std::cout << Fibonacci::get(n) << std::endl;

	return 0;
}